package io.ubermut.conway.model;

/**
 * Class represents Conway Game constants.
 */
public class Conway {

    // game states
    public static final String STATE = "game::state";
    public static final String PLAYER_COLOR = "game::player_color";
    public static final String PLAYER_CLICK = "game::player_click";
    public static final String PLAYER_PATTERN = "game::player_pattern";
    public static final String PLAYERS_ONLINE = "game::players_online";

    // game patterns
    public static final String BEEHIVE = "beehive";
    public static final String TOAD = "toad";
    public static final String LWSS = "lwss";
    public static final String GILDER = "glider";

}
