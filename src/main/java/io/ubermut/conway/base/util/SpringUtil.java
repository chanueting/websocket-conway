package io.ubermut.conway.base.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * A Spring utility class wrapping the functionality provided by the ApplicationContext and exposing them as static
 * methods. This saves the application from obtaining a reference to the ApplicationContext in every place where it is
 * needed.
 */
@Component
public class SpringUtil implements ApplicationContextAware {
    /**
     * Represents the reference to the underlying application context.
     */
    private static ApplicationContext applicationContext;

    /**
     * Gets the reference to the underlying application context.
     *
     * @return the reference to the underlying application context.
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * Sets the reference to the underlying application context.
     *
     * @param applicationContext the reference to the underlying application context.
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        SpringUtil.applicationContext = applicationContext;
    }

    /**
     * Gets the bean based on the given expected class of the bean. If something goes wrong when the method is invoked,
     * the method throws RuntimException (see the ApplicationContext#getBean method for details).
     *
     * @param <T> the type of the bean.
     * @param clazz the expected class of the bean.
     * @return an instance of the bean.
     */
    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    /**
     * Gets the bean based on the given name of the bean, with the given expected class. If something goes wrong when
     * the method is invoked, the method throws RuntimException (see the ApplicationContext#getBean method for details).
     *
     * @param <T> the type of the bean.
     * @param name the name of the bean.
     * @param clazz the expected class of the bean.
     * @return an instance of the bean.
     */
    public static <T> T getBean(String name, Class<T> clazz) {
        return applicationContext.getBean(name, clazz);
    }

    /**
     * Gets the bean based on the given name of the bean, with the given arguments for the constructor. If something
     * goes wrong when the method is invoked, the method throws RuntimException (see the ApplicationContext#getBean
     * method for details).
     *
     * @param <T> the type of the bean.
     * @param name the name of the bean.
     * @param args the arguments for the constructor.
     * @return an instance of the bean.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(String name, Object... args) {
        return (T) applicationContext.getBean(name, args);
    }
}
