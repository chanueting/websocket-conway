package io.ubermut.conway.service;

import io.ubermut.conway.model.Conway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.awt.*;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ConwayGameService {

    /**
     * Represents the logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ConwayGameService.class);

    /**
     * Represents the collection of websocket sessions.
     */
    private final ConcurrentHashMap<String, WebSocketSession> sessions = new ConcurrentHashMap<>();

    /**
     * Represents the attribute field name of colour used in WebSocketSession.
     */
    private String colourAttribute = "colour";

    /**
     * Represents a strong random number generator.
     */
    private SecureRandom rand = new SecureRandom();

    /**
     * Represents the ConwayGame service.
     */
    @Autowired
    private ConwayGame game;

    /**
     * Represents the ConwayTimer service.
     */
    @Autowired
    private ConwayTimer conwayTimer;

    /**
     * Return a random colour in int (abgr).
     *
     * @return a random colour in int.
     */
    private int getRandomColor() {
        float hue = rand.nextFloat();
        // sat between 0.2 and 1.0
        float saturation = rand.nextInt(10000 + 1 - 2000) + 2000 / 10000f;
        float luminance = 0.9f;
        Color color = Color.getHSBColor(hue, saturation, luminance);

        return color.getAlpha() << 24 | color.getBlue() << 16 | color.getGreen() << 8 | color.getRed();
    }

    /**
     * Set a random colour to the websocket session.
     *
     * @param session WebSocketSession
     */
    private void setSessionColour(WebSocketSession session) {
        int colour = getRandomColor();
        session.getAttributes().put(colourAttribute, colour);
    }

    /**
     * Retrieve the colour allocated to the websocket session.
     *
     * @param session WebSocketSession
     * @return the colour of this session
     */
    public int getSessionColour(WebSocketSession session) {
        return (int) session.getAttributes().getOrDefault(colourAttribute, 1);
    }

    /**
     * Add a websocket session in the game.
     * Start the game if there was no session.
     *
     * @param session websocket session
     * @return CompletableFuture<Void>
     */
    public CompletableFuture<Void> addSession(WebSocketSession session) {
        if (sessions.isEmpty()) {
            conwayTimer.startTimer();
        }
        setSessionColour(session);
        sessions.put(session.getId(), session);
        return null;
    }

    /**
     * Return the number of websocket sessions.
     *
     * @return the number of websocket sessions.
     */
    public int getSessionsCount() {
        return sessions.size();
    }

    /**
     * Remove the websocket session.
     *
     * @param session websocket session.
     * @return CompletableFuture<Void>
     */
    public CompletableFuture<Void> removeSession(WebSocketSession session) {
        sessions.remove(session.getId());

        if (sessions.isEmpty()) {
            conwayTimer.stopTimer();
        }
        return null;
    }

    /**
     * Trigger player action on the game board, and the board would be halted for easier input.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param session websocket session
     * @return CompletableFuture<Void>
     */
    public CompletableFuture<Void> playerAction(int x, int y, WebSocketSession session) {
        conwayTimer.interruptTimer();
        game.playerClick(x, y, getSessionColour(session));
        return null;
    }

    /**
     * Trigger to draw a beehive on the game board.
     *
     * @param session websocket session
     * @return CompletableFuture<Void>
     */
    public CompletableFuture<Void> drawBeehive(WebSocketSession session) {
        game.drawBeehive(getSessionColour(session));
        return null;
    }

    /**
     * Trigger to draw a toad on the game board.
     *
     * @param session websocket session
     * @return CompletableFuture<Void>
     */
    public CompletableFuture<Void> drawToad(WebSocketSession session) {
        game.drawToad(getSessionColour(session));
        return null;
    }

    /**
     * Trigger to draw a lwss on the game board.
     *
     * @param session websocket session
     * @return CompletableFuture<Void>
     */
    public CompletableFuture<Void> drawLwss(WebSocketSession session) {
        game.drawLwss(getSessionColour(session));
        return null;
    }

    /**
     * Trigger to draw a glider on the game board.
     *
     * @param session websocket session
     * @return CompletableFuture<Void>
     */
    public CompletableFuture<Void> drawGlider(WebSocketSession session) {
        game.drawGlider(getSessionColour(session));
        return null;
    }

    /**
     * Trigger to draw a glider on the game board.
     */
    public void tick() throws Exception {
        game.tick();
        broadcastBoard();
    }

    /**
     * Trigger to draw a glider on the game board.
     */
    public void broadcastBoard() {
        // broadcast the board
        int[] colours = game.getColours();
        broadcast(String.format("{\"type\": \"%s\", \"data\" : %s}", Conway.STATE, Arrays.toString(colours)));
    }

    /**
     * Send a message to all websocket sessions.
     *
     * @param message message to be broadcasted to all websocket sessions.
     */
    public void broadcast(String message) {
        TextMessage msg = new TextMessage(message);
        for (WebSocketSession session : sessions.values()) {
            try {
                session.sendMessage(msg);
            }
            catch (Throwable ex) {
                log.error("Broadcast exception. ", ex);
                // if session#sendMessage fails the client is removed
                removeSession(session);
            }
        }
    }

    /**
     * Send a message to a particular websocket session.
     *
     * @param session websocket session.
     * @param message message to be sent.
     */
    public void sendMessage(WebSocketSession session, String message) {
        try {
            session.sendMessage(new TextMessage(message));
        }
        catch (Throwable ex) {
            log.error("Broadcast exception. ", ex);
            // if session#sendMessage fails the client is removed
            removeSession(session);
        }
    }
}
