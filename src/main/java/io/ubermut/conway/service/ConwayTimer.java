package io.ubermut.conway.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Timer for the multi-player webSocket sessions.
 */
@Service
public class ConwayTimer {

    /**
     * Represents the logger.
     */
    private final long TICK_DELAY = 500;

    private final long TICK_DELAY_INTERRUPT = 500;

    /**
     * Represents the logger.
     */
    private final Logger log = LoggerFactory.getLogger(ConwayTimer.class);

    /**
     * Represents timer of the game.
     */
    private Timer gameTimer = null;

    /**
     * Represents the Conway game service.
     */
    @Autowired
    private ConwayGameService gameService;

    /**
     * Handles the interruption on the timer, especially there is player action.
     */
    public void interruptTimer() {
        gameTimer.cancel();
        gameTimer = new Timer(ConwayTimer.class.getSimpleName() + " Timer");
        gameTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    gameService.tick();
                }
                catch (Throwable ex) {
                    log.error("Caught to prevent timer from shutting down", ex);
                }
            }
        }, TICK_DELAY_INTERRUPT, TICK_DELAY);
    }

    /**
     * Timer start for the game.
     */
    public void startTimer() {
        gameTimer = new Timer(ConwayTimer.class.getSimpleName() + " Timer");
        gameTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    gameService.tick();
                }
                catch (Throwable ex) {
                    log.error("Caught to prevent timer from shutting down", ex);
                }
            }
        }, TICK_DELAY, TICK_DELAY);
    }

    /**
     * Timer stop as the game stop.
     */
    public void stopTimer() {
        if (gameTimer != null) {
            gameTimer.cancel();
        }
    }
}