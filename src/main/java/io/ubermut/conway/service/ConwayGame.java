package io.ubermut.conway.service;

import io.ubermut.conway.base.Pair;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.IntStream;

@Service
public class ConwayGame {

    // default colour of the cells
    private static final int COLOUR_BLANK = -1;

    // default number of columns on the board
    private static final int COLS = 150;

    // default number of rows on the board
    private static final int ROWS = 100;

    // integer colour values for each cell
    private int[] colours = IntStream.generate(() -> COLOUR_BLANK).limit(COLS * ROWS).toArray();

    // colours data for next state
    private int[] nextStateColours = IntStream.generate(() -> COLOUR_BLANK).limit(COLS * ROWS).toArray();

    // strong random number generator
    private static SecureRandom rand = new SecureRandom();

    /**
     * Return list of colours in cells
     */
    public int[] getColours() {
        return Arrays.copyOf(colours, colours.length);
    }

    /**
     * Returns colour of cell at x,y
     *
     * @param x x-coordinate of cell
     * @param y y-coordinate of cell
     * @return integer colour of cell at x,y
     */
    public int getColour(int x, int y) {
        return this.colours[COLS * y + x];
    }

    /**
     * Sets color of cell at x,y
     *
     * @param x      x-coordinate of cell
     * @param y      y-coordinate of cell
     * @param colour integer colour of cell at x,y
     */
    public void setColour(int x, int y, int colour) {
        this.colours[COLS * y + x] = colour;
    }

    /**
     * Returns life of cell at x,y
     *
     * @param x x-coordinate of cell
     * @param y y-coordinate of cell
     * @return true if cell is alive, otherwise false
     */
    public boolean isAlive(int x, int y) {
        return (this.colours[COLS * y + x] != COLOUR_BLANK);
    }

    /**
     * When player clicks the canvas
     * Resets gameTick timeout to avoid interrupting player while they are clicking cells
     * If the clicked cell is dead, makes it alive and gives it the player's colour
     *
     * @param x      x-coordinate of cell
     * @param y      y-coordinate of cell
     * @param colour player's colour
     */
    public void playerClick(int x, int y, int colour) {
        if (!this.isAlive(x, y)) {
            this.setColour(x, y, colour);
        }
    }

    /**
     * Sets colour of cell at x,y next state
     *
     * @param x      x-coordinate of cell
     * @param y      y-coordinate of cell
     * @param colour integer colour of cell
     */
    public void setNextStateColour(int x, int y, int colour) {
        this.nextStateColours[COLS * y + x] = colour;
    }

    /**
     * Replaces colour and lives states with their corresponding next states
     * Reset next states for colours and lives
     */
    public void nextState() {
        // swap arrays
        int[] tempColours = this.colours;
        this.colours = this.nextStateColours;
        this.nextStateColours = tempColours;

        // re-init the arrays
        Arrays.fill(nextStateColours, COLOUR_BLANK);
    }


    /**
     * Returns number of live neighbors for a given cell and colours of alive neighbors
     * There's some indexing arithmetic to make the world toroidal (opposite edges are connected)
     *
     * @param x x-coordinate of cell
     * @param y y-coordinate of cell
     * @return pair of number of alive neighbors and colours of all live neighbors
     */
    public Pair<Integer, int[]> countNeighbours(int x, int y) {
        int[] colours = new int[8];
        int count = 0;
        int nx;
        int ny;

        // northwest neighbour
        ny = y - 1 < 0 ? ROWS - 1 : y - 1;
        nx = x - 1 < 0 ? COLS - 1 : x - 1;
        if (this.isAlive(nx, ny)) {
            colours[count] = this.getColour(nx, ny);
            count++;
        }

        // north neighbour
        nx = x;
        if (this.isAlive(nx, ny)) {
            colours[count] = this.getColour(nx, ny);
            count++;
        }

        // northeast neighbour
        nx = (x + 1 == COLS) ? 0 : x + 1;
        if (this.isAlive(nx, ny)) {
            colours[count] = this.getColour(nx, ny);
            count++;
        }

        // east neighbour
        ny = y;
        if (this.isAlive(nx, ny)) {
            colours[count] = this.getColour(nx, ny);
            count++;
        }

        // west neighbour
        nx = (x - 1 < 0) ? COLS - 1 : x - 1;
        if (this.isAlive(nx, ny)) {
            colours[count] = this.getColour(nx, ny);
            count++;
        }

        // southwest neighbour
        ny = (y + 1 == ROWS) ? 0 : y + 1;
        if (this.isAlive(nx, ny)) {
            colours[count] = this.getColour(nx, ny);
            count++;
        }

        // south neighbour
        nx = x;
        if (this.isAlive(nx, ny)) {
            colours[count] = this.getColour(nx, ny);
            count++;
        }

        // southeast neighbour
        nx = (x + 1 == COLS) ? 0 : x + 1;
        if (this.isAlive(nx, ny)) {
            colours[count] = this.getColour(nx, ny);
            count++;
        }

        colours = Arrays.copyOfRange(colours, 0, count);
        return new Pair<>(count, colours);
    }

    /**
     * Runs Game of Life algorithm every X milliseconds
     * Uses setTimeout to allow for dynamic interval logic
     * e.g. pending timeout is reset and longer interval is applied when a player clicks on the game
     */
    public void tick() {
        for (int y = 0; y < ROWS; y++) {
            for (int x = 0; x < COLS; x++) {
                Pair<Integer, int[]> neighbours = countNeighbours(x, y);

                int count = neighbours.first;

                // Any live cell with fewer than two live neighbours dies,
                // as if caused by under-population.
                if (count < 2 && this.isAlive(x, y)) {
                    this.setNextStateColour(x, y, COLOUR_BLANK);
                }

                // Any live cell with two or three live neighbours lives on to the next generation.
                if ((count == 2 || count == 3) && this.isAlive(x, y)) {
                    this.setNextStateColour(x, y, this.getColour(x, y));
                }

                // Any live cell with more than three live neighbours dies, as if by overcrowding.
                if (count > 3 && this.isAlive(x, y)) {
                    this.setNextStateColour(x, y, COLOUR_BLANK);
                }

                // Any dead cell with exactly three live neighbours becomes a live cell,
                // as if by reproduction.
                if (count == 3 && !this.isAlive(x, y)) {
                    this.setNextStateColour(x, y, averageColour(neighbours.second));
                }
            }
        }

        this.nextState();
    }

    /**
     * Returns function object of converting integer colour to Color object
     */
    public Function<Integer, Color> intToColour = colour -> {
        int r = colour & 0xff;
        int g = (colour >> 8) & 0xff;
        int b = (colour >> 16) & 0xff;
        int a = ((colour >> 24) & 0xff);
        return new Color(r, g, b, a);
    };

    /**
     * Returns average colour of input colours
     * Takes into account logarithmic scale of brightness
     * https://www.youtube.com/watch?v=LKnqECcg6Gw
     *
     * @param colours array of integer colour values
     * @return average colour value
     */
    protected int averageColour(int[] colours) {
        int sumR = 0;
        int sumG = 0;
        int sumB = 0;
        int sumA = 0;

        for (int colour : colours) {
            int r = colour & 0xff;
            int g = (colour >> 8) & 0xff;
            int b = (colour >> 16) & 0xff;
            int a = ((colour >> 24) & 0xff);

            sumR += r * r;
            sumG += g * g;
            sumB += b * b;
            sumA += a;
        }

        int r = (int) Math.sqrt(sumR / colours.length);
        int g = (int) Math.sqrt(sumG / colours.length);
        int b = (int) Math.sqrt(sumB / colours.length);
        int a = (sumA / colours.length);

        return (a << 24) | (b << 16) | (g << 8) | r;
    }


    /**
     * Draws a beehive pattern
     *
     * @param colour the colour of the pattern
     */
    protected void drawBeehive(int colour) {
        int x = rand.nextInt(COLS - 5 + 1 - 5) + 5;
        int y = rand.nextInt(ROWS - 5 + 1 - 5) + 5;

        this.setColour(x, y - 1, colour);

        this.setColour(x - 1, y, colour);

        this.setColour(x, y + 1, colour);

        this.setColour(x + 1, y - 1, colour);

        this.setColour(x + 2, y, colour);

        this.setColour(x + 1, y + 1, colour);
    }

    /**
     * Draws a toad pattern
     *
     * @param colour the colour of the pattern
     */
    protected void drawToad(int colour) {
        int x = rand.nextInt(COLS - 5 + 1 - 5) + 5;
        int y = rand.nextInt(ROWS - 5 + 1 - 5) + 5;

        this.setColour(x, y, colour);

        this.setColour(x, y + 1, colour);

        this.setColour(x - 1, y + 1, colour);

        this.setColour(x + 1, y, colour);

        this.setColour(x + 1, y + 1, colour);

        this.setColour(x + 2, y, colour);
    }

    /**
     * Draws a lwss pattern
     *
     * @param colour the colour of the pattern
     */
    protected void drawLwss(int colour) {
        int x = rand.nextInt(COLS - 5 + 1 - 5) + 5;
        int y = rand.nextInt(ROWS - 5 + 1 - 5) + 5;

        this.setColour(x - 1, y + 1, colour);

        this.setColour(x - 1, y + 3, colour);

        this.setColour(x, y, colour);

        this.setColour(x + 1, y, colour);

        this.setColour(x + 2, y, colour);

        this.setColour(x + 3, y, colour);

        this.setColour(x + 3, y + 1, colour);

        this.setColour(x + 3, y + 2, colour);

        this.setColour(x + 2, y + 3, colour);
    }


    /**
     * Draws a glider pattern
     *
     * @param colour the colour of the pattern
     */
    protected void drawGlider(int colour) {
        int x = rand.nextInt(COLS - 5 + 1 - 5) + 5;
        int y = rand.nextInt(ROWS - 5 + 1 - 5) + 5;

        this.setColour(x, y, colour);
        this.setColour(x + 1, y, colour);
        this.setColour(x + 2, y, colour);
        this.setColour(x + 2, y - 1, colour);
        this.setColour(x + 1, y - 2, colour);
    }
}
