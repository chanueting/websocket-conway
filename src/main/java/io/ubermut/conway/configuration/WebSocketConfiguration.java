package io.ubermut.conway.configuration;

import io.ubermut.conway.controller.ConwayWebSocketHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.PerConnectionWebSocketHandler;

@Configuration
@EnableScheduling
@EnableWebSocket
public class WebSocketConfiguration implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(conwayWebSocketHandler(), "/conway").setAllowedOrigins("*")
                .withSockJS();
    }

    @Bean
    public WebSocketHandler conwayWebSocketHandler() {
        return new PerConnectionWebSocketHandler(ConwayWebSocketHandler.class);
    }
}
