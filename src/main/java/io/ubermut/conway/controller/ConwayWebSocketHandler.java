package io.ubermut.conway.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.ubermut.conway.base.util.SpringUtil;
import io.ubermut.conway.model.Conway;
import io.ubermut.conway.service.ConwayGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class ConwayWebSocketHandler extends TextWebSocketHandler {

    /**
     * Represents the Conway game service.
     */
    @Autowired
    private ConwayGameService conwayGameService = SpringUtil.getBean(ConwayGameService.class);

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        conwayGameService.addSession(session);

        // boardcast number of players
        conwayGameService.broadcast(String.format("{\"type\": \"%s\", \"data\" : [%s]}", Conway.PLAYERS_ONLINE, conwayGameService.getSessionsCount()));

        // send player's colour
        conwayGameService.sendMessage(session, String.format("{\"type\": \"%s\", \"data\" : %s}", Conway.PLAYER_COLOR, conwayGameService.getSessionColour(session)));
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message)
            throws Exception {
        String payload = message.getPayload();

        // parse json
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(payload);
        String event = jsonNode.path("event").asText();

        if (event.equals(Conway.PLAYER_CLICK)) {
            JsonNode position = jsonNode.path("position");
            int x = position.path("x").asInt();
            int y = position.path("y").asInt();
            conwayGameService.playerAction(x, y, session);
        } else if (event.equals(Conway.PLAYER_PATTERN)) {
            String pattern = jsonNode.path("pattern").asText();
            switch (pattern) {
                case Conway.BEEHIVE:
                    conwayGameService.drawBeehive(session);
                    break;
                case Conway.TOAD:
                    conwayGameService.drawToad(session);
                    break;
                case Conway.LWSS:
                    conwayGameService.drawLwss(session);
                    break;
                case Conway.GILDER:
                    conwayGameService.drawGlider(session);
                    break;
            }
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        conwayGameService.removeSession(session);

        // boardcast number of players
        conwayGameService.broadcast(String.format("{\"type\": \"%s\", \"data\" : [%s]}", Conway.PLAYERS_ONLINE, conwayGameService.getSessionsCount()));
    }
}
