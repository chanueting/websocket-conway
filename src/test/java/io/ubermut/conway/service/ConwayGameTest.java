package io.ubermut.conway.service;

import io.ubermut.conway.base.Pair;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.util.ReflectionTestUtils;

import java.awt.Color;
import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class ConwayGameTest {

    /**
     * Represents the default colour of the cells.
     *
     * @see ConwayGame#COLOUR_BLANK
     */
    private static final int COLOUR_BLANK = -1;

    /**
     * Represents the default number of columns on the board.
     *
     * @see ConwayGame#COLS
     */
    private static final int COLS = 150;

    /**
     * Represents the default number of rows on the board.
     *
     * @see ConwayGame#ROWS
     */
    private static final int ROWS = 100;

    /**
     * Represents the colour red in hex.
     */
    private final int red = 0xff0000ff;

    /**
     * Represents the colour black in hex.
     */
    private final int black = 0xff000000;

    /**
     * Represents the colour blue in hex.
     */
    private final int blue = 0xffff0000;

    @Autowired
    private ConwayGame game;

    @Mock
    private SecureRandom mockRandom = Mockito.mock(SecureRandom.class);

    @Test
    @DisplayName("Get colours list")
    public void testGetColours() {
        final int redX = 0;
        final int redY = 0;
        final int blackX = 1;
        final int blackY = 1;
        game.setColour(redX, redY, red);
        game.setColour(blackX, blackY, black);
        int[] colours = game.getColours();
        assertEquals(red, colours[COLS * redY + redX]);
        assertEquals(black, colours[COLS * blackY + blackX]);
    }

    @Test
    @DisplayName("Get a copy of colours instead of the original")
    public void getUnmodifiableColours() {
        int[] colours = game.getColours();
        assertEquals(COLOUR_BLANK, colours[0]);
        colours[0] = red;
        colours = game.getColours();
        assertEquals(COLOUR_BLANK, colours[0]);
    }

    @Test
    @DisplayName("Set and Get same colour")
    public void setGetColour() {
        game.setColour(0, 0, red);
        assertEquals(red, game.getColour(0, 0));
    }

    @Test
    @DisplayName("Set colour and is life")
    public void setIsLife() {
        game.setColour(0, 0, red);
        assertEquals(true, game.isAlive(0, 0));
    }

    @Test
    @DisplayName("Play click life cell")
    public void testPlayerClickLifeCell() {
        final int x = 0;
        final int y = 0;
        game.setColour(0, 0, red);
        game.playerClick(x, y, black);
        assertEquals(red, game.getColour(x, y));
        assertEquals(true, game.isAlive(x, y));
    }

    @Test
    @DisplayName("Play click dead cell")
    public void testPlayerClickDeadCell() {
        final int x = 0;
        final int y = 0;
        game.setColour(x, y, COLOUR_BLANK);
        game.playerClick(x, y, red);
        assertNotEquals(COLOUR_BLANK, game.getColour(x, y));
        assertEquals(red, game.getColour(x, y));
        assertEquals(true, game.isAlive(x, y));
    }

    @Test
    @DisplayName("No neighbours by default ")
    public void testCountDefaultNeighbours() {
        final int x = 0;
        final int y = 0;

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(COLOUR_BLANK, game.getColour(x, y));
        assertEquals(0, neighbours.first);
        assertEquals(0, neighbours.second.length);
    }

    @Test
    @DisplayName("No neighbours by default even clicked")
    public void testCountDefaultNeighboursEvenClicked() {
        final int x = 0;
        final int y = 0;

        game.playerClick(x, y, red);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertNotEquals(COLOUR_BLANK, game.getColour(x, y));
        assertEquals(red, game.getColour(x, y));
        assertEquals(0, neighbours.first);
        assertEquals(0, neighbours.second.length);
    }

    @Test
    @DisplayName("One northwest neighbour")
    public void testCountNorthwestNeighbour() {
        final int x = 1;
        final int y = 1;
        final int nx = x - 1;
        final int ny = y - 1;

        game.playerClick(nx, ny, red);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(red, game.getColour(nx, ny));
        assertEquals(1, neighbours.first);
        assertEquals(1, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
    }

    @Test
    @DisplayName("One north neighbour")
    public void testCountNorthNeighbour() {
        final int x = 1;
        final int y = 1;
        final int nx = x;
        final int ny = y - 1;

        game.playerClick(nx, ny, red);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(red, game.getColour(nx, ny));
        assertEquals(1, neighbours.first);
        assertEquals(1, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
    }

    @Test
    @DisplayName("One northeast neighbour")
    public void testCountNorthEastNeighbour() {
        final int x = 1;
        final int y = 1;
        final int nx = x + 1;
        final int ny = y - 1;

        game.playerClick(nx, ny, red);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(red, game.getColour(nx, ny));
        assertEquals(1, neighbours.first);
        assertEquals(1, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
    }

    @Test
    @DisplayName("Three north neighbours (northwest, north, northeast)")
    public void testCountThreeNorthNeighbours() {
        final int x = 1;
        final int y = 1;

        // northwest
        int nx = x - 1;
        int ny = y - 1;

        game.playerClick(nx, ny, red);

        // north
        nx = x;
        ny = y - 1;

        game.playerClick(nx, ny, black);

        // northeast
        nx = x + 1;
        ny = y - 1;

        game.playerClick(nx, ny, blue);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(3, neighbours.first);
        assertEquals(3, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
        assertEquals(black, neighbours.second[1]);
        assertEquals(blue, neighbours.second[2]);
    }

    @Test
    @DisplayName("One east neighbour")
    public void testCountEastNeighbour() {
        final int x = 1;
        final int y = 1;
        final int nx = x + 1;
        final int ny = y;

        game.playerClick(nx, ny, red);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(red, game.getColour(nx, ny));
        assertEquals(1, neighbours.first);
        assertEquals(1, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
    }

    @Test
    @DisplayName("One west neighbour")
    public void testCountWestNeighbour() {
        final int x = 1;
        final int y = 1;
        final int nx = x - 1;
        final int ny = y;

        game.playerClick(nx, ny, red);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(red, game.getColour(nx, ny));
        assertEquals(1, neighbours.first);
        assertEquals(1, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
    }

    @Test
    @DisplayName("One southwest neighbour")
    public void testCountSouthWestNeighbour() {
        final int x = 1;
        final int y = 1;
        final int nx = x - 1;
        final int ny = y + 1;

        game.playerClick(nx, ny, red);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(red, game.getColour(nx, ny));
        assertEquals(1, neighbours.first);
        assertEquals(1, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
    }

    @Test
    @DisplayName("One south neighbour")
    public void testCountSouthNeighbour() {
        final int x = 1;
        final int y = 1;
        final int nx = x;
        final int ny = y + 1;

        game.playerClick(nx, ny, red);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(red, game.getColour(nx, ny));
        assertEquals(1, neighbours.first);
        assertEquals(1, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
    }

    @Test
    @DisplayName("One southeast neighbour")
    public void testCountSouthEastNeighbour() {
        final int x = 1;
        final int y = 1;
        final int nx = x + 1;
        final int ny = y + 1;

        game.playerClick(nx, ny, red);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(red, game.getColour(nx, ny));
        assertEquals(1, neighbours.first);
        assertEquals(1, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
    }


    @Test
    @DisplayName("Three south neighbours (southwest, south, southeast)")
    public void testCountThreeSouthNeighbours() {
        final int x = 1;
        final int y = 1;

        // southwest
        int nx = x - 1;
        int ny = y + 1;

        game.playerClick(nx, ny, red);

        // south
        nx = x;
        ny = y + 1;

        game.playerClick(nx, ny, black);

        // southeast
        nx = x + 1;
        ny = y + 1;

        game.playerClick(nx, ny, blue);

        Pair<Integer, int[]> neighbours = game.countNeighbours(x, y);

        assertEquals(3, neighbours.first);
        assertEquals(3, neighbours.second.length);
        assertEquals(red, neighbours.second[0]);
        assertEquals(black, neighbours.second[1]);
        assertEquals(blue, neighbours.second[2]);
    }

    /**
     * Toad should be resume original after two ticks:
     * <p>
     * (original)       (one tick)       (original)
     * [0,0,0,0,0,0]    [0,0,0,x,0,0]    [0,0,0,0,0,0]
     * [0,0,x,x,x,0]    [0,x,0,0,x,0]    [0,0,x,x,x,0]
     * [0,x,x,x,0,0] => [0,x,0,0,x,0] => [0,x,x,x,0,0]
     * [0,0,0,0,0,0]    [0,0,x,0,0,0]    [0,0,0,0,0,0]
     * [O,0,0,0,0,0]    [O,0,0,0,0,0]    [O,0,0,0,0,0]
     * <p>
     * (x, y) = (2, 2) for example
     *
     * @see #testTickWithToad
     */
    @Test
    @DisplayName("Tick with Toad")
    public void testTickWithToad() {
        final int x = 2;
        final int y = 2;
        ReflectionTestUtils.setField(game, "rand", mockRandom);
        Mockito.when(mockRandom.nextInt(Mockito.anyInt()))
                .thenReturn(x - 5).thenReturn(y - 5);
        game.drawToad(red);

        // original toad
        assertEquals(red, game.getColour(x, y));
        assertEquals(true, game.isAlive(x, y));

        assertEquals(red, game.getColour(x, y + 1));
        assertEquals(true, game.isAlive(x, y + 1));

        assertEquals(red, game.getColour(x - 1, y + 1));
        assertEquals(true, game.isAlive(x - 1, y + 1));

        assertEquals(red, game.getColour(x + 1, y));
        assertEquals(true, game.isAlive(x + 1, y));

        assertEquals(red, game.getColour(x + 1, y + 1));
        assertEquals(true, game.isAlive(x + 1, y + 1));

        assertEquals(red, game.getColour(x + 2, y));
        assertEquals(true, game.isAlive(x + 2, y));

        // one tick
        game.tick();

        assertEquals(red, game.getColour(x - 1, y));
        assertEquals(true, game.isAlive(x - 1, y));

        assertEquals(red, game.getColour(x - 1, y + 1));
        assertEquals(true, game.isAlive(x - 1, y + 1));

        assertEquals(red, game.getColour(x, y + 2));
        assertEquals(true, game.isAlive(x, y + 2));

        assertEquals(red, game.getColour(x + 1, y - 1));
        assertEquals(true, game.isAlive(x + 1, y - 1));

        assertEquals(red, game.getColour(x + 2, y));
        assertEquals(true, game.isAlive(x + 2, y));

        assertEquals(red, game.getColour(x + 2, y + 1));
        assertEquals(true, game.isAlive(x + 2, y + 1));

        // another tick
        game.tick();

        // back to original state
        assertEquals(red, game.getColour(x, y));
        assertEquals(true, game.isAlive(x, y));

        assertEquals(red, game.getColour(x, y + 1));
        assertEquals(true, game.isAlive(x, y + 1));

        assertEquals(red, game.getColour(x - 1, y + 1));
        assertEquals(true, game.isAlive(x - 1, y + 1));

        assertEquals(red, game.getColour(x + 1, y));
        assertEquals(true, game.isAlive(x + 1, y));

        assertEquals(red, game.getColour(x + 1, y + 1));
        assertEquals(true, game.isAlive(x + 1, y + 1));

        assertEquals(red, game.getColour(x + 2, y));
        assertEquals(true, game.isAlive(x + 2, y));
    }


    /**
     * Glider should be moved after one tick:
     * <p>              (one tick)
     * [0,0,0,x,0,0]    [0,0,0,0,0,0]
     * [0,0,0,0,x,0]    [0,0,x,0,x,0]
     * [0,0,x,x,x,0] => [0,0,0,x,x,0]
     * [0,0,0,0,0,0]    [0,0,0,x,0,0]
     * [0,0,0,0,0,0]    [O,0,0,0,0,0]
     * <p>
     * * (x, y) = (2, 2) for example
     *
     * @see #testDrawGlider
     */
    @Test
    @DisplayName("Tick with Glider")
    public void testTickWithGlider() {
        final int x = 2;
        final int y = 2;
        ReflectionTestUtils.setField(game, "rand", mockRandom);
        Mockito.when(mockRandom.nextInt(Mockito.anyInt()))
                .thenReturn(x - 5).thenReturn(y - 5);
        game.drawGlider(red);

        // original glider
        assertEquals(red, game.getColour(x, y));
        assertEquals(true, game.isAlive(x, y));

        assertEquals(red, game.getColour(x + 1, y));
        assertEquals(true, game.isAlive(x + 1, y));

        assertEquals(red, game.getColour(x + 2, y));
        assertEquals(true, game.isAlive(x + 2, y));

        assertEquals(red, game.getColour(x + 2, y - 1));
        assertEquals(true, game.isAlive(x + 2, y - 1));

        assertEquals(red, game.getColour(x + 1, y - 2));
        assertEquals(true, game.isAlive(x + 1, y - 2));

        // one tick
        game.tick();

        assertEquals(red, game.getColour(x + 1, y));
        assertEquals(true, game.isAlive(x + 1, y));

        assertEquals(red, game.getColour(x + 2, y));
        assertEquals(true, game.isAlive(x + 2, y));

        assertEquals(red, game.getColour(x + 2, y - 1));
        assertEquals(true, game.isAlive(x + 2, y - 1));

        assertEquals(red, game.getColour(x + 1, y + 1));
        assertEquals(true, game.isAlive(x + 1, y + 1));

        assertEquals(red, game.getColour(x, y - 1));
        assertEquals(true, game.isAlive(x, y - 1));
    }

    @Test
    @DisplayName("0xff0000ff is Red")
    public void testIntRedToColour() {
        Color redColour = game.intToColour.apply(red);
        assertEquals(redColour, Color.RED);
    }

    @Test
    @DisplayName("0xff000000 is Black")
    public void testIntBlackToColour() {
        Color blackColour = game.intToColour.apply(black);
        assertEquals(blackColour, Color.BLACK);
    }

    @Test
    @DisplayName("0xffff0000 is Blue")
    public void testIntGreenToColour() {
        Color blueColour = game.intToColour.apply(blue);
        assertEquals(blueColour, Color.BLUE);
    }

    @Test
    @DisplayName("0xff0000B4 is average of Red and Black")
    public void testAverageColour() {
        assertEquals(0xff0000B4, game.averageColour(new int[] {red, black}));
    }

    @Test
    @DisplayName("Draw Beehive")
    /**
     * Beehive:
     * [0,0,0,0,0,0]
     * [0,0,x,x,0,0]
     * [0,x,0,0,x,0]
     * [0,0,x,x,0,0]
     * [O,0,0,0,0,0]
     *
     * (x, y) = (2, 2) for example
     *
     * drawBeehive drift to x + 5, y + 5 avoiding IndexOutOfBoundsException
     *
     * @see ConwayGame#drawBeehive
     */
    public void testDrawBeehive() {
        final int x = 2;
        final int y = 2;
        ReflectionTestUtils.setField(game, "rand", mockRandom);
        Mockito.when(mockRandom.nextInt(Mockito.anyInt()))
                .thenReturn(x - 5).thenReturn(y - 5);
        game.drawBeehive(red);

        assertEquals(red, game.getColour(x, y - 1));
        assertEquals(true, game.isAlive(x, y - 1));

        assertEquals(red, game.getColour(x - 1, y));
        assertEquals(true, game.isAlive(x - 1, y));

        assertEquals(red, game.getColour(x, y + 1));
        assertEquals(true, game.isAlive(x, y + 1));

        assertEquals(red, game.getColour(x + 1, y - 1));
        assertEquals(true, game.isAlive(x + 1, y - 1));

        assertEquals(red, game.getColour(x + 2, y));
        assertEquals(true, game.isAlive(x + 2, y));

        assertEquals(red, game.getColour(x + 1, y + 1));
        assertEquals(true, game.isAlive(x + 1, y + 1));
    }

    @Test
    @DisplayName("Draw Toad")
    /**
     * Toad:
     * [0,0,0,0,0,0]
     * [0,0,x,x,x,0]
     * [0,x,x,x,0,0]
     * [0,0,0,0,0,0]
     * [O,0,0,0,0,0]
     *
     * * (x, y) = (2, 2) for example
     *
     * drawToad drift to x + 5, y + 5 avoiding IndexOutOfBoundsException
     *
     * @see ConwayGame#drawToad
     */
    public void testDrawToad() {
        final int x = 2;
        final int y = 2;
        ReflectionTestUtils.setField(game, "rand", mockRandom);
        Mockito.when(mockRandom.nextInt(Mockito.anyInt()))
                .thenReturn(x - 5).thenReturn(y - 5);
        game.drawToad(red);

        assertEquals(red, game.getColour(x, y));
        assertEquals(true, game.isAlive(x, y));

        assertEquals(red, game.getColour(x, y + 1));
        assertEquals(true, game.isAlive(x, y + 1));

        assertEquals(red, game.getColour(x - 1, y + 1));
        assertEquals(true, game.isAlive(x - 1, y + 1));

        assertEquals(red, game.getColour(x + 1, y));
        assertEquals(true, game.isAlive(x + 1, y));

        assertEquals(red, game.getColour(x + 1, y + 1));
        assertEquals(true, game.isAlive(x + 1, y + 1));

        assertEquals(red, game.getColour(x + 2, y));
        assertEquals(true, game.isAlive(x + 2, y));
    }

    @Test
    @DisplayName("Draw Lwss")
    /**
     * Lwss:
     * [0,0,0,0,0,0]
     * [0,0,x,x,x,x]
     * [0,x,0,0,0,x]
     * [0,0,0,0,0,x]
     * [0,x,0,0,x,0]
     *
     * * (x, y) = (2, 3) for example
     *
     * drawLwss drift to x + 5, y + 5 avoiding IndexOutOfBoundsException
     *
     * @see ConwayGame#drawLwss
     */
    public void testDrawLwss() {
        final int x = 2;
        final int y = 3;
        ReflectionTestUtils.setField(game, "rand", mockRandom);
        Mockito.when(mockRandom.nextInt(Mockito.anyInt()))
                .thenReturn(x - 5).thenReturn(y - 5);
        game.drawLwss(red);

        assertEquals(red, game.getColour(x - 1, y + 1));
        assertEquals(true, game.isAlive(x - 1, y + 1));

        assertEquals(red, game.getColour(x - 1, y + 3));
        assertEquals(true, game.isAlive(x - 1, y + 3));

        assertEquals(red, game.getColour(x, y));
        assertEquals(true, game.isAlive(x, y));

        assertEquals(red, game.getColour(x + 1, y));
        assertEquals(true, game.isAlive(x + 1, y));

        assertEquals(red, game.getColour(x + 2, y));
        assertEquals(true, game.isAlive(x + 2, y));

        assertEquals(red, game.getColour(x + 3, y));
        assertEquals(true, game.isAlive(x + 3, y));

        assertEquals(red, game.getColour(x + 3, y + 1));
        assertEquals(true, game.isAlive(x + 3, y + 1));

        assertEquals(red, game.getColour(x + 3, y + 2));
        assertEquals(true, game.isAlive(x + 3, y + 2));

        assertEquals(red, game.getColour(x + 2, y + 3));
        assertEquals(true, game.isAlive(x + 2, y + 3));
    }

    @Test
    @DisplayName("Draw Glider")
    /**
     * Glider:
     * [0,0,0,x,0,0]
     * [0,0,0,0,x,0]
     * [0,0,x,x,x,0]
     * [0,0,0,0,0,0]
     * [0,0,0,0,0,0]
     *
     * * (x, y) = (2, 2) for example
     *
     * drawGlider drift to x + 5, y + 5 avoiding IndexOutOfBoundsException
     *
     * @see ConwayGame#drawGlider
     */
    public void testDrawGlider() {
        final int x = 2;
        final int y = 2;
        ReflectionTestUtils.setField(game, "rand", mockRandom);
        Mockito.when(mockRandom.nextInt(Mockito.anyInt()))
                .thenReturn(x - 5).thenReturn(y - 5);
        game.drawGlider(red);

        assertEquals(red, game.getColour(x, y));
        assertEquals(true, game.isAlive(x, y));

        assertEquals(red, game.getColour(x + 1, y));
        assertEquals(true, game.isAlive(x + 1, y));

        assertEquals(red, game.getColour(x + 2, y));
        assertEquals(true, game.isAlive(x + 2, y));

        assertEquals(red, game.getColour(x + 2, y - 1));
        assertEquals(true, game.isAlive(x + 2, y - 1));

        assertEquals(red, game.getColour(x + 1, y - 2));
        assertEquals(true, game.isAlive(x + 1, y - 2));
    }
}
