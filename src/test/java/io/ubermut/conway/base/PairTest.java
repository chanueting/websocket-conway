package io.ubermut.conway.base;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PairTest {

    @Test
    @DisplayName("Verify equals and hashCode")
    public void equalsContract() {
        EqualsVerifier.forClass(Pair.class).verify();
    }

    @Test
    @DisplayName("Verify toString")
    public void testToString() {
        assertEquals("Pair{1 1}", Pair.create(1, 1).toString());
    }

    @Test
    @DisplayName("Create Pair is same as Pair constructor")
    public void testCreate() throws Exception {
        assertEquals(new Pair(1, 1), Pair.create(1, 1));
    }
}