# This is the cache image
FROM gradle:6.6.1-jdk8 AS cache
RUN mkdir -p /home/gradle/cache_home
ENV GRADLE_USER_HOME /home/gradle/cache_home
COPY --chown=gradle:gradle build.gradle /home/gradle/
WORKDIR /home/gradle
RUN gradle resolveDependencies --info

# This is the builder image
FROM gradle:6.6.1-jdk8 AS builder
COPY --from=cache --chown=gradle:gradle /home/gradle/cache_home /home/gradle/.gradle
COPY --chown=gradle:gradle . /home/gradle
WORKDIR /home/gradle
RUN gradle assemble --info

# Seperate between dependencies and application resources
# https://spring.io/guides/gs/spring-boot-docker/
# Seperate layers with layered jars
# https://spring.io/blog/2020/01/27/creating-docker-images-with-spring-boot-2-3-0-m1?#layered-jars
RUN mkdir -p build/dependency && (cd build/dependency; java -Djarmode=layertools -jar ../libs/*.jar extract)

# This is the image for the conway app
FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG GRADLE_WORKDIR=/home/gradle
ARG DEPENDENCY=${GRADLE_WORKDIR}/build/dependency
WORKDIR application
COPY --from=builder ${DEPENDENCY}/dependencies/ ./
COPY --from=builder ${DEPENDENCY}/snapshot-dependencies/ ./
#COPY --from=builder ${DEPENDENCY}/resources/ ./
COPY --from=builder ${DEPENDENCY}/application/ ./
COPY --from=builder ${DEPENDENCY}/spring-boot-loader/ .
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]