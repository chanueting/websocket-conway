# Multiplayer Conway's Game of Life 
Sample site on Heroku:

https://websocket-conway.herokuapp.com/

## INTRODUCTION 
The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970.

It is originated a zero-player game, a grid input is now provided to click and add cells or patterns. 

## Running from Source

Java 8 is required. After installed, execute the command in your console to start the game

```
$ ./gradlew bootRun
```

Visit http://localhost:8080 and enjoy playing!

### Technical details
- SockJS is used to provide cross-browser Websocket communication with servers.
- Spring Boot is used to provide server side service, which the game logic resides on.

### Architectural Assessment
- SockJS is chosen as there is no Node.js dependency, just retrieve from CDN then it is good to go.
- Spring Boot is chosen as it is easy to make standalone Java application with Spring framework.
 
### Tradeoff made
- There is no reconnection using SockJS solely, using other libraries like socket.io could allows players continue play the game without page refresh.

### Acknowledgments
- [Spring Boot Websocket Undertow Sample for Spring Boot 1.5.X](https://github.com/spring-projects/spring-boot/tree/1.5.x/spring-boot-samples/spring-boot-sample-websocket-undertow)
- [Spring Boot Smoke test for Spring Boot 2.2.X](https://github.com/spring-projects/spring-boot/tree/2.2.x/spring-boot-tests/spring-boot-smoke-tests/spring-boot-smoke-test-websocket-undertow)
- [Another Multiplayer Conway's Game of Life in Node.js](https://github.com/prashcr/multiplayer-conway)

